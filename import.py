import requests
import tarfile
import pymongo
import json
import sys
import os

from google.protobuf.message import DecodeError
from google.protobuf.json_format import MessageToJson
sys.path.insert(0, 'protobuf')
import gtfs_realtime_pb2 as gtfsrt

TAR_INPUT_DIR = "tar"

config = {}

if os.environ.get('API_URL') is not None:
	parameters = {'source':os.environ.get('DATASET')}
	req = requests.get(os.environ.get('API_URL')+"/status/config/db-url", params=parameters, timeout=10).json()
	config['DB_URL'] = os.environ.get('DB_URL', req['result'])
	config['PATH_TAR'] = os.environ.get('PATH_TAR', TAR_INPUT_DIR)
else:
	config['DB_URL'] = os.environ.get('DB_URL')
	config['PATH_TAR'] = os.environ.get('PATH_TAR', TAR_INPUT_DIR)

client = pymongo.MongoClient(config['DB_URL'])
db = client.get_database()

for table_name in ['alerts', 'trip_updates', 'vehicle_positions']:
	timestamp_index_exist = False
	if table_name in db.collection_names():
		index_info = db[table_name].index_information()
		for index in index_info:
			for key in index_info[index]['key']:
				for field in key:
					if field == "header.timestamp":
						timestamp_index_exist = True
	if not timestamp_index_exist:
		print("No Timestamp Index Located for "+table_name+". Creating...")
		db[table_name].create_index(
			[("header.timestamp", pymongo.DESCENDING)],
			unique=True,background=True
		)

for file in sorted(os.listdir(config['PATH_TAR'])):
	if file.endswith('.tar.gz'):
		print(file)
		with tarfile.open(os.path.join(config['PATH_TAR'], file), "r:gz") as tar:
			for pb_file in tar.getnames():

				if pb_file.endswith('_alerts.pb'):
					table_name = 'alerts'
				elif pb_file.endswith('_vehicle_positions.pb'):
					table_name = 'vehicle_positions'
				elif pb_file.endswith('_trip_updates.pb'):
					table_name = 'trip_updates'
				else:
					print('Unexpected File. '+pb_file)
					exit()

				fm = gtfsrt.FeedMessage()
				fm.ParseFromString(tar.extractfile(pb_file).read())
				data = json.loads(MessageToJson(fm))

				data['header']['timestamp'] = int(data['header']['timestamp'])
				try:
					db[table_name].insert_one(data)
					print(str(data['header']['timestamp']),"- DB Inserted to "+table_name+".")
				except pymongo.errors.DuplicateKeyError:
					print(str(data['header']['timestamp']),"- DB Rejected to "+table_name+". Duplicate Keys.")






